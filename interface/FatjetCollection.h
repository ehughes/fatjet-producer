#ifndef Producers_FatjetCollection_h
#define Producers_FatjetCollection_h

#include <vector>
#include "fastjet/PseudoJet.hh"
//#include "DataFormats/Common/interface/SortedCollection.h"

// this is our new product, it is simply a 
// collection of PseudoJet held in an std::vector
namespace edm {
	typedef std::vector<fastjet::PseudoJet> FatjetCollection;
}

#endif
