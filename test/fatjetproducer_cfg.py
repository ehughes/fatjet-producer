# Author: Elliot Hughes
# Last update: 141204

## ------------------------------------------------------
# Commonly-changed user parameters:
## ------------------------------------------------------

# PARAMETERS
# Enter them below for now

## ------------------------------------------------------
#
## ------------------------------------------------------


# IMPORTS
import re
import FWCore.ParameterSet.Config as cms			# Standard config file information.
from FWCore.ParameterSet.VarParsing import VarParsing		# Module used to pass arguments from the command line.
#from PhysicsTools.PatAlgos.tools.pfTools import *		# Deep in here (PhysicsTools.PatAlgos.tools.jetTools) is addJetCollection.
from Configuration.AlCa.autoCond import autoCond		# For automatically determining global tags

# Imported input files:
# from sttojj_100gev import *

# DEFINE ARGUMENTS
options = VarParsing('analysis')
options.register ('outDir',
	'',
	VarParsing.multiplicity.singleton,
	VarParsing.varType.string,
	"Output directory"
)
options.register ('outFile',
	'',
	VarParsing.multiplicity.singleton,
	VarParsing.varType.string,
	"Output file"
)
options.register ('inDir',
	'',
	VarParsing.multiplicity.singleton,
	VarParsing.varType.string,
	"Input directory"
)
options.register ('inFile',
	'',
	VarParsing.multiplicity.singleton,
	VarParsing.varType.string,
	"Input file"
)
# defaults:
options.maxEvents = 2
options.outDir = "/uscms_data/d3/tote/CMSSW_7_0_9_patch2/src/Producers/FatjetProducer/test/data"
options.inDir = "/uscms_data/d3/tote/CMSSW_7_0_9_patch2/src/Producers/FatjetProducer/test/data"
# set up:
options.parseArguments()
options.inFile = "sqtojj_100gev_7_chs_709.root"
options.outFile = "sqtojj_100gev_7_chs_fatjets_709.root"

# CONSTRUCT
process = cms.Process("fatjets")

# DEFINE CONFIGURATION
# Reports
process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.options = cms.untracked.PSet(
	wantSummary = cms.untracked.bool(False)			# Long summary after job.
)


# Construct analyzer
process.FatjetProducer = cms.EDProducer("FatjetProducer",
#	v = cms.bool(False),		# Verbose mode
#	R = cms.double(1.2),		# CA fatjet distance parameter (cone size)
#	make_gen = cms.bool(True),		# Make gen fatjets
#	make_pf = cms.bool(True),		# Make PF fatjets
#	do_prune = cms.bool(False),		# Applying pruning
#	do_trim = cms.bool(True),		# Apply trimming
#	trim_R = cms.double(0.2),		# Trimming parameter
#	trim_ptf = cms.double(0.03)		# Trimming parameter
)

# Input
process.source = cms.Source("PoolSource",
	fileNames = cms.untracked.vstring(
		"file:{0}/{1}".format(options.inDir, options.inFile)
	),
#	eventsToProcess = cms.untracked.VEventRange('1:44929-1:44938'),
)
# Events
process.maxEvents = cms.untracked.PSet(
	input = cms.untracked.int32(options.maxEvents)
)
# Output
process.out = cms.OutputModule("PoolOutputModule",
	fileName = cms.untracked.string("{0}/{1}".format(options.outDir, options.outFile)),
#	SelectEvents = cms.untracked.PSet( SelectEvents = cms.vstring('p') ),	# Save only events passing the full pathway
#	outputCommands = cms.untracked.vstring(		# https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideSelectingBranchesForOutput
#		"drop *",
##		"keep *_*_*_fatjets"
#		"keep *_*_*_*"
#	)
)

#process.TFileService = cms.Service("TFileService",
#	fileName = cms.string("{0}/{1}.root".format(options.outDir, out_file1))
#)

# Define path
process.p = cms.Path(
	process.FatjetProducer
)
process.e = cms.EndPath(
	process.out
)


