// Author: Elliot Hughes
// Last update: 141204
// https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCreatingNewProducts
// https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookEDMTutorialProducer

// INCLUDES
// System includes
#include <iostream>
#include <typeinfo>

// User includes
//// Basic includes
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDProducer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/Common/interface/Handle.h"
#include "FWCore/Framework/interface/ESHandle.h"

//// Important class includes
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/Math/interface/deltaPhi.h"
#include "DataFormats/JetReco/interface/BasicJet.h"
#include "DataFormats/JetReco/interface/BasicJetCollection.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/ParticleFlowCandidate/interface/PFCandidate.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"		// JetDefinition, ClusterSequence, etc.
//#include "fastjet-contrib/Nsubjettiness/Nsubjettiness.hh"
#include "fastjet/tools/Pruner.hh"
#include "fastjet/Selector.hh"
#include "fastjet/tools/Filter.hh"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "Producers/FatjetProducer/interface/FatjetCollection.h"

//// Meta includes
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"

//// ROOT includes
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TFile.h"
//#include "TFileDirectory.h"
#include "TTree.h"
#include "TBranch.h"
#include "TNtuple.h"

// TEST
#include "DataFormats/Math/interface/Point3D.h"
// \INCLUDES

// CLASS DEFINITION
class FatjetProducer : public edm::EDProducer {
	public:
		explicit FatjetProducer(const edm::ParameterSet&);
		~FatjetProducer();
		
		static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

	private:
		virtual void beginJob(const edm::EventSetup&) ;
		virtual void produce(edm::Event&, const edm::EventSetup&);
		virtual void endJob() ;
		
		//virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
		//virtual void endRun(edm::Run const&, edm::EventSetup const&) override;
		//virtual void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;
		//virtual void endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;

	// Member data
	edm::InputTag src_; 
//	typedef fastjet::PseudoJet Fatjet;
//	typedef std::vector<fastjet::PseudoJet> PseudoJetCollection;
//    typedef math::XYZPointD Point;
//    typedef std::vector<Point> PointCollection;
};

// Constructors and destructors
FatjetProducer::FatjetProducer(const edm::ParameterSet& iConfig)
{
   //register your products
/* Examples
   produces<ExampleData2>();

   //if do put with a label
   produces<ExampleData2>("label");
*/
   //now do what ever other initialization is needed

//	src_  = iConfig.getParameter<edm::InputTag>( "src" ); 
//	produces<PointCollection>( "innerPoint" ).setBranchAlias( "innerPoints");
//	produces<PseudoJetCollection>( "thing" ).setBranchAlias( "fatjetsPF" );
	std::cout << "HERE 97" << std::endl;
	produces<edm::FatjetCollection>( "thing" ).setBranchAlias( "fatjetsPF" );
	std::cout << "HERE 99" << std::endl;
//	produces<Fatjet>( "thing" ).setBranchAlias( "fatjetsPF" );
//	produces<fastjet::PseudoJet>( "thing" ).setBranchAlias( "fatjetsPF" );

}
FatjetProducer::~FatjetProducer()
{
 
	std::cout << "HERE 107" << std::endl;
   // do anything here that needs to be done at desctruction time
   // (e.g. close files, deallocate resources etc.)

}


// CLASS METHODS

// ------------ method called to produce the data  ------------
void FatjetProducer::produce(
	edm::Event& iEvent,
	const edm::EventSetup& iSetup
){
	std::cout << "HERE 120" << std::endl;
	using namespace edm; 
	using namespace reco; 
	using namespace std;
	using namespace fastjet;
	cout << "HERE 108" << endl;
//   Handle<TrackCollection> tracks;
	Handle<PFCandidateCollection> objects_pf;
	iEvent.getByLabel("particleFlow", objects_pf);
	// create the vectors. Use auto_ptr, as these pointers will automatically
	// delete when they go out of scope, a very efficient way to reduce memory leaks.
	auto_ptr<FatjetCollection> fatjetsPF( new FatjetCollection );
//	auto_ptr<PseudoJet> fatjetsPF( new PseudoJet );
	vector<PseudoJet> jobjects_pf;
	for (PFCandidateCollection::const_iterator object = objects_pf->begin(); object != objects_pf->end(); ++ object) {
		jobjects_pf.push_back( PseudoJet(object->px(), object->py(), object->pz(), object->energy()) );
	}
	ClusterSequence cs = ClusterSequence(jobjects_pf, JetDefinition(cambridge_algorithm, 1.2));		// Perform the clustering with FastJet.
	*fatjetsPF.get() = sorted_by_pt(cs.inclusive_jets());
//	fatjetsPF->reserve( objects_pf->size() );
	cout << "HERE 120" << endl;
//	auto_ptr<FatjetCollection> fjptr = &fatjetsPF;		// This is not how things work...
	cout << "HERE 140" << endl;
   // loop over the tracks:
	for( PFCandidateCollection::const_iterator object = objects_pf->begin(); object != objects_pf->end(); ++ object ) {
		// fill the fatjets in the vectors
//		fatjetsPF->push_back( fastjet::PseudoJet(object->px(), object->py(), object->pz(), object->energy()) );
	}
	// and save the vectors
	cout << "HERE 126" << endl;
	iEvent.put( fatjetsPF, "fatjetsPF" );
	cout << "HERE 128" << endl;
}

// ------------ method called once each job just before starting event loop  ------------
void 
FatjetProducer::beginJob(const edm::EventSetup&)
{
	std::cout << "HERE 149" << std::endl;
}

// ------------ method called once each job just after ending the event loop  ------------
void 
FatjetProducer::endJob() {
  std::cout << "HERE 155" << std::endl;
}

// ------------ method called when starting to processes a run  ------------
/*
void
FatjetProducer::beginRun(edm::Run const&, edm::EventSetup const&)
{
}
*/
 
// ------------ method called when ending the processing of a run  ------------
/*
void
FatjetProducer::endRun(edm::Run const&, edm::EventSetup const&)
{
}
*/
 
// ------------ method called when starting to processes a luminosity block  ------------
/*
void
FatjetProducer::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/
 
// ------------ method called when ending the processing of a luminosity block  ------------
/*
void
FatjetProducer::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/
 
// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
FatjetProducer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
  std::cout << "HERE 197" << std::endl;
}

//define this as a plug-in
DEFINE_FWK_MODULE(FatjetProducer);


