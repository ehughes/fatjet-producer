#include "Producers/FatjetProducer/interface/FatjetCollection.h"
#include "DataFormats/Common/interface/Wrapper.h"

namespace Producers_Products {
	struct dictionary {
		fastjet::PseudoJet						dummy2;
		std::vector<fastjet::PseudoJet*>		dummy3;
		std::vector<fastjet::PseudoJet>			dummy4;
		edm::FatjetCollection						dummy0;
		edm::Wrapper<edm::FatjetCollection>			dummy1;
	};
}
